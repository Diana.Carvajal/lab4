package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {
  private Rectangle2D box;
  private GridDimension gd;    // Esta clase necesitará ser definida para manejar las dimensiones de la cuadrícula.
  private double margin; 

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  public Rectangle2D getBoundsForCell(CellPosition cp){
    double totalMarginWidth = (gd.cols() + 1) * margin;
    double totalMarginHeight = (gd.rows() + 1)* margin;
    double cellWidth = (box.getWidth() - totalMarginWidth) / gd.cols();
    double cellHeight = (box.getHeight() - totalMarginHeight) / gd.rows();
    double cellX = box.getX() + margin + (cp.col() * (cellWidth + margin));
    double cellY = box.getY() + margin + (cp.row() * (cellHeight + margin));
    return new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
      
 
    }

  }

