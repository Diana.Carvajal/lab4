package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import javax.swing.JFrame;
import java.awt.Color;

public class Main {
  public static void main(String[] args) {
    // Opprett ColorGrid objekt
    ColorGrid colorGrid = new ColorGrid(3, 4);
    
 // Sett fargene i hjørnene
    colorGrid.set(new CellPosition(0, 0), Color.RED);
    colorGrid.set(new CellPosition(0, 3), Color.BLUE);
    colorGrid.set(new CellPosition(2, 0), Color.YELLOW);
    colorGrid.set(new CellPosition(2, 3), Color.GREEN);
    
    //Opprett et GridView-objekt med det opprettede ColorGrid-objektet
    GridView gridview = new GridView(colorGrid);

    // Opprett et JFrame-objekt
    JFrame frame = new JFrame();

    // Kall setContentPane-metoden med GridView-objektet som argument
    frame.setContentPane(gridview);

    // Kall setTitle, setDefaultCloseOperation, pack og setVisible 
    frame.setTitle("Grid View");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
}
