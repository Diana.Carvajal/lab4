package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D; 
import java.awt.Dimension;
import javax.swing.JPanel;
import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;
import no.uib.inf101.colorgrid.GridDimension;


public class GridView extends JPanel {
  private final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
  private IColorGrid colorGrid;
  
  public GridView(IColorGrid colorGrid) {
    this.setPreferredSize(new Dimension(400, 300));
    this.colorGrid = colorGrid;
  }

  @Override
  public void paintComponent(Graphics g){
    super.paintComponent(g);
    //opprett en Graphics2D-variabel fra g; why?
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2); 
    // here can I call drawCells(g2) if needed
  }
     // Velg din favoritt-figur og tegn den midlertidig
    //g2.drawOval(50, 50, 100, 100);
  

//implementer tegning av rutenett
  private void drawGrid (Graphics2D g2){
    double x = OUTERMARGIN;
    double y = OUTERMARGIN;
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    Rectangle2D box = new Rectangle2D.Double(x, y, width, height);
    g2.setColor(MARGINCOLOR);  
    g2.fill(box); 

    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(box, colorGrid, OUTERMARGIN);
    drawCells(g2, colorGrid, converter);
  }

  private static void drawCells(Graphics2D g2, CellColorCollection color , CellPositionToPixelConverter converter){
    for (CellColor cell : color.getCells()) {
      Rectangle2D box = converter.getBoundsForCell(cell.cellPosition());
      g2.setColor(cell.color() != null ? cell.color() : Color.DARK_GRAY);
      g2.fill(box);
    }
}
}

