package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;


public class ColorGrid implements IColorGrid {
  private final int ROWS, COLS;
  private final List<List<Color>> GRID;

  public ColorGrid (int rows, int cols){
    this.ROWS = rows;
    this.COLS = cols;
    this.GRID = new ArrayList<>();    //initializer rutenett 
  
      for(int row = 0; row < ROWS; row++){
        ArrayList<Color> rowList = new ArrayList<>();
        for(int col = 0; col < COLS; col++){
          rowList.add(null); 

        }
        this.GRID.add(rowList);
      }
    }



 //ROWS / COLS / GET CELLS / 
  @Override
  public int rows() {
    return this.ROWS;
  }

  @Override
  public int cols() {
    return this.COLS;
  }



  @Override
  public List<CellColor> getCells() {
    List<CellColor> cellColors = new ArrayList<>();
    for (int row = 0; row < ROWS; row++) {
        for (int col = 0; col < COLS; col++) {
            CellPosition pos = new CellPosition(row, col);
            Color color = get(pos);
            cellColors.add(new CellColor(pos, color));
        }
    }

    return cellColors;
}


  // GET AND SET

  //return the color of the cell
  @Override
  public Color get(CellPosition pos) {
    int row = pos.row();
    int col = pos.col();

      if (row < 0 || row >= ROWS || col < 0 || col >= COLS){
        throw new IndexOutOfBoundsException ("The position is out of bounds");
      }

    return this.GRID.get(row).get(col);
  }


  @Override
  public void set(CellPosition pos, Color color) {
    int row = pos.row();
    int col = pos.col();

    if (row < 0 || row >= ROWS || col < 0 || col >= COLS) {
      throw new IndexOutOfBoundsException("The position is out of bounds");    
    }
    
    this.GRID.get(row).set(col,color);

//*     List<CellColor> cellColors = this.GRID.get(row);
//    CellColor cellColor = new CellColor(pos, color);
//    cellColors.set(col, cellColor);
//  }
  }
}